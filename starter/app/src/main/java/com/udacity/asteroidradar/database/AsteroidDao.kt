package com.udacity.asteroidradar.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface AsteroidDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(asteroid: AsteroidEntity) // <= This needs to be the entity name

    @Query ("SELECT * FROM asteroids_table ORDER BY closeApproachDate DESC")
    fun getAsteroidsFromDB(): LiveData<List<AsteroidEntity>> // should this be a flow?

}