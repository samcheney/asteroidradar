package com.udacity.asteroidradar.main

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import com.udacity.asteroidradar.database.AsteroidRepository
import com.udacity.asteroidradar.database.AsteroidRoomDatabase
import com.udacity.asteroidradar.network.AsteroidResponse

class MainViewModel(application: Application) : AndroidViewModel(application) {
    private val database by lazy { AsteroidRoomDatabase.getDatabase(getApplication()) }
    private val repository by lazy { AsteroidRepository(database.asteroidDao) }

    //LUAN simple variable to call the repository flow and convert it in VM to LiveData
    //https://developer.android.com/codelabs/android-room-with-a-view-kotlin#9
    val asteroids: LiveData<AsteroidResponse> = repository.getAsteroids().asLiveData()
}