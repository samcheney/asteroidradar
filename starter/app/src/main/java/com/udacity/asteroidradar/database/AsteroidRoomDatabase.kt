package com.udacity.asteroidradar.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

// Annotates class to be a Room Database with a table (entity) of the Word class
@Database(entities = [AsteroidEntity::class], version = 1, exportSchema = false)
abstract class AsteroidRoomDatabase : RoomDatabase() {

    abstract val asteroidDao: AsteroidDao

    companion object {
        // set up singleton database instance
        @Volatile // may accidentally be read by two or more threads
        private var INSTANCE: AsteroidRoomDatabase? = null

        fun getDatabase(context: Context): AsteroidRoomDatabase {
            // if the INSTANCE is not null, return it
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AsteroidRoomDatabase::class.java,
                    "asteroid_database"
                ).build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}

