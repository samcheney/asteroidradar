package com.udacity.asteroidradar.database

import com.udacity.asteroidradar.network.AsteroidApi
import com.udacity.asteroidradar.network.AsteroidResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class AsteroidRepository (private val asteroidDao: AsteroidDao) {

    //LUAN Simple flow builder to emit the API response
    fun getAsteroids(): Flow<AsteroidResponse> {
        return flow {
            emit(AsteroidApi.retrofitService.getAsteroids())
        }.flowOn(Dispatchers.IO) // Use the IO thread for this Flow
    }
}