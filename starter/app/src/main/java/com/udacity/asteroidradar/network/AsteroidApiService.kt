package com.udacity.asteroidradar.network

import com.udacity.asteroidradar.Constants.API_KEY
import com.udacity.asteroidradar.Constants.BASE_URL
import com.udacity.asteroidradar.PictureOfDay
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface AsteroidApiService {
    @GET("planetary/apod?api_key=$API_KEY")
    //TODO suspend fun
    suspend fun getPictureOfTheDay(): PictureOfDay

    //LUAN keep it simple. Add the key in the url for now
    @GET("neo/rest/v1/feed?api_key=$API_KEY")
    suspend fun getAsteroids(): AsteroidResponse
}

object AsteroidApi {
    //LUAN Only include this to have nice HttpOk logging in the Logcat
    val retrofitService : AsteroidApiService by lazy {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .client(client)
            .build()

        retrofit.create(AsteroidApiService::class.java)
    }
}