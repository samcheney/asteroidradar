# Asteroid Radar

Asteroid Radar is a android app designed to warn us of our impending doom! Check in every day to see the list of the close approaches of nearby asteroids and a picture of the day.

## Installation

To install the app, either clone the repo into your own project or download the zip file on bitbucket (currently a private repo). 

## Usage

In order to run this app you will need to have a physical devise or an emulator running minimum skd of 21.

## Design

This app is composed of the following elements:
 
API service: fetches data from the NASA.gov and composes the JSON into Kotlin objects.

Dao: sets up an interface over SQLiteOpenHelper().



## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)